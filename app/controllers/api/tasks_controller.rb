# frozen_string_literal: true

class Api::TasksController < ApplicationController
  # GET /tasks
  def index
    @tasks = Task.order("updated_at DESC")
    render json: { tasks: @tasks }
  end

  # POST /tasks
  def create
    @task = Task.new(create_task_params)

    if @task.save
      render json: { task: @task }, status: :ok
    else
      render json: { errors: @task.errors.full_messages }, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /tasks/1
  def update
    @task = Task.find(params[:id])
    if @task.update(update_task_params)
      render json: { task: @task }, status: :ok
    else
      render json: { errors: @task.errors.full_messages }, status: :unprocessable_entity
    end
  end

  private

  def create_task_params
    params.require(:task).permit(:name)
  end

  def update_task_params
    params.require(:task).permit(:is_done)
  end
end
