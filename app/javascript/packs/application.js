//= require jquery
//= require materialize

require("@rails/ujs").start()
require("@rails/activestorage").start()
require("channels")
