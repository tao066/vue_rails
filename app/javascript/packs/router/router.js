// require vue
import Vue from 'vue/dist/vue.esm.js'
import VueRouter from 'vue-router'

// require components
import Index   from '../components/index.vue'
import About   from '../components/about.vue'
import Contact from '../components/contact.vue'

// require router
Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: Index },
    { path: '/about', component: About },
    { path: '/contact', component: Contact },
  ],
})
